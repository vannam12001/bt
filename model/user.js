function User(
  id,
  name,
  username,
  email,
  street,
  suite,
  city,
  zipcode,
  geo,
  lat,
  lng,
  phone,
  website,
  companyName,
  companyCatchPhrase,
  companyBs
) {
  this.id = id;
  this.name = name;
  this.username = username;
  this.email = email;
  this.street = street;
  this.suite = suite;
  this.city = city;
  this.zipcode = zipcode;
  this.geo = geo;
  this.lat = lat;
  this.lng = lng;
  this.phone = phone;
  this.website = website;
  this.companyName = companyName;
  this.companyCatchPhrase = companyCatchPhrase;
  this.companyBs = companyBs;
}
